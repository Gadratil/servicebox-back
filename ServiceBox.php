<?php 

use ServiceBox\Interfaces\Storage as StorageInterface;
use ServiceBox\Interfaces\Validation as ValidationInterface;
use ServiceBox\Interfaces\Purifier as PurifierInterface;
use ServiceBox\Storage\DB as DBStorage;
use ServiceBox\Validation\Wall as ValidationWall;
use ServiceBox\Purifier;
use ServiceBox\Config;
use ServiceBox\Plugger;
use ServiceBox\Auth\Auth as Auth;
use ServiceBox\Auth\User;
use ServiceBox\Request;
use ServiceBox\Request\System as SystemRequest;
use ServiceBox\Request\Service as ServiceRequest;
use ServiceBox\Request\Handler as RequestHandler;
use ServiceBox\Response;
use ServiceBox\Response\General as GeneralResponse;

class ServiceBox
{
	private $_storage = null;
	
	private $_validation = null;
	
	private $_config = null;
	
	private $_purifier = null;
	
	private $_auth = null;
	
	private $_requestHandler = null;
	
	private $_service = null;
	
	public function __construct(
		StorageInterface $storage = null, 
		ValidationInterface $validation = null, 
		PurifierInterface $purifier = null
	)
	{
		$this->_config = new Config();
		
		$this->_storage = ( $storage == null )? new DBStorage($this->_config->db) : $storage;
		
		$this->_validation = ( $validation == null )? new ValidationWall() : $validation;
		
		$this->_purifier = ( $purifier == null )? new Purifier() : $purifier;
		
		$this->_auth = new Auth($this->_storage);
		
		$this->_requestHandler = new RequestHandler($this->_storage, $this->_validation, $this->_auth);
		//$this->_auth->register('admin', 'q1w2e3r4', 'attila.ordog@yahoo.com');
	}
	
	public function check_request()
	{
		
		if ( $_POST )
		{
			$post = $this->_purifier->purify($_POST);
			
			$request = new SystemRequest();
			if ( isset($post['type']) AND $post['type'] == 1 )
			{
				$request = new ServiceRequest();
			}
			
			$request->inject_data($post);
			
			$response = $this->_requestHandler->handle($request);
			
			echo json_encode((array)$response);
			exit;
		}
	}
}