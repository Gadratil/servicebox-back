<?php

namespace ServiceBox;

use ServiceBox\Interfaces\Purifier as PurifierInterface;
use HTMLPurifier;

class Purifier implements PurifierInterface
{
	public function purify(Array $data)
	{
		$this->_purify($data);
		
		return $data;
	}
	
	private function _purify($data)
	{
		if ( is_array($data) )
		{
			foreach ( $data as $key => $value )
			{
				$data[$key] = $this->_purify($value);
			}
		}
		else
		{
			return HTMLPurifier::instance()->purify($data);
		}
	}
}