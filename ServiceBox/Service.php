<?php

namespace ServiceBox;

abstract class Service
{
	public $slug = '';
	
	/**
	 * Gets an entity belonging to the service by its name
	 * @param string $object_type
	 * @return Object
	 */
	public function get_entity_by_object_type($object_type)
	{
		if ( file_exists(SERVICEBOX_ROOT.'services/'.$this->slug.'/entities/'.$object_type.'.php') )
		{
			$instance = include_once(SERVICEBOX_ROOT.'services/'.$this->slug.'/entities/'.$object_type.'.php');
			
			return $instance;
		}
		
		return null;
	}
	
	/**
	 * Runs a custom action belonging to the service
	 * These functions have only one param and that is an array with whatever is needed to pass
	 * @param string $custom_action Name of the action to take
	 * @param array $data The data that needs to be passed to the function
	 * @return mixed
	 */
	public function run_custom_function($custom_action, Array $data = array())
	{
		if ( !method_exists($this, $custom_action) )
		{
			return null;
		}
		
		return $this->$custom_action($data);
	}
}