<?php

namespace ServiceBox\Validation\Rule;

use ValidatonWall;
use ValidationWall\Rule\Rule as Rule;

class AlphaNumeric extends Rule
{
	public function validate($field, Array $data = array())
	{
		$this->_error_message = $field.' alphanumeric';
		
		// Only check if available
		if ( !array_key_exists($field, $data) )
		{
			return true;
		}
		
		$this->_variable = $data[$field];
		
		return ctype_alnum($this->_variable);
	}
}
?>