<?php

namespace ServiceBox\Validation;

use ServiceBox\Interfaces\Validation as ValidationInterface;
use ServiceBox\Entity;
use ServiceBox\Validation\MainDoor;
use ServiceBox\Validation\MainRuleset;
use ServiceBox\Validation\Rule\AlphaNumeric;
use ValidationWall;
use ValidationWall\Rule\NotEmpty;
use ValidationWall\Rule\Numeric;

class Wall implements ValidationInterface
{
	private $_errors = array();
	
	public function valid(Entity $entity, Array $data, $action)
	{
		$rulesets = array();
		
		$val_def = $entity->get_validation_definition();
		if ( array_key_exists($action, $val_def) )
		{
			$val_def = $val_def[$action];
		}
		else
		{
			if ( array_key_exists('all', $val_def) )
			{
				$val_def = $val_def['all'];
			}
			else
			{
				$val_def = array();
			}
		}
		
		foreach ( $val_def as $key => $rules )
		{
			if ( !empty($rules) )
			{
				$rulesets[] = new MainRuleset($key, $rules);
			}
		}
		
		$door = new MainDoor($rulesets);
		
		$vw = new ValidationWall($door);
		
		if ( $vw->pass($data) )
		{
			return true;
		}
		else
		{
			$this->_errors = $door->getErrors();
			return false;
		}
	}
	
	public function get_errors()
	{
		return $this->_errors;
	}
}