<?php

namespace ServiceBox\Action;

use ServiceBox\Action;
use ServiceBox\Plugger;
use ServiceBox\ServiceBoxException;
use ServiceBox\Response\General as GeneralResponse;

class Service extends Action
{
	public $_plugger = null;
	public $_service = null;
	
	public function perform()
	{
		$this->_response = new GeneralResponse();
		$this->_plugger = new Plugger();
		$this->_service = $this->_plugger->get_service($this->_request->service);
		
		if ( $this->_service === null )
		{
			$this->_response->msg = 'service_not_found';
			throw new ServiceBoxException('service_not_found');
		}
	}
}