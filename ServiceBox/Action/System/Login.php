<?php

namespace ServiceBox\Action\System;

use ServiceBox\Action;
use ServiceBox\Auth\User;

class Login extends Action
{
	public function perform()
	{
		$sess_id = isset($_SESSION['sess_id'])? $_SESSION['sess_id'] : '';
		
		if ( $this->_auth->is_logged_in($sess_id) )
		{
			$this->_response->success = true;
			$this->_response->msg = 'login_success';
			return $this->_response;
		}
		
		$user = new User();
		$user->inject_data($this->_request->data);
		
		$valid = $this->_validation->valid($user, $this->_request->data, $this->_request->action);
		
		if ( $valid )
		{
			if ( $this->_auth->login($this->_request->data['username'], $this->_request->data['password']) )
			{
				$this->_response->success = true;
				$this->_response->msg = 'login_success';
			}
			else
			{
				$this->_response->msg = 'login_fail';
				$this->_response->errors = $this->_auth->get_errors();
			}
		}
		else
		{
			$this->_response->msg = 'login_fail';
			$this->_response->errors = $this->_validation->get_errors();
		}
	}
}	