<?php

namespace ServiceBox\Action\System;

use ServiceBox\Action;

class IsLoggedIn extends Action
{
	public function perform()
	{
		$sess_id = isset($_SESSION['sess_id'])? $_SESSION['sess_id'] : '';
		
		$this->_response->success = $this->_auth->is_logged_in($sess_id);
	}
}