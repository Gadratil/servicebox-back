<?php

namespace ServiceBox\Action\System;

use ServiceBox\Action;

class Logout extends Action
{
	public function perform()
	{
		$sess_id = isset($_SESSION['sess_id'])? $_SESSION['sess_id'] : '';
		
		$this->_auth->logout($sess_id);
			
		$this->_response->success = true;
		$this->_response->msg = 'logout_success';
	}
}