<?php

namespace ServiceBox\Action;

use ServiceBox\Action\Service as ServiceAction;
use ServiceBox\ServiceBoxException;

class Entity extends ServiceAction
{
	public $_entity = null;
	
	public function perform()
	{
		parent::perform();
		
		$this->_entity = $this->_service->get_entity_by_object_type($this->_request->objectType);
			
		if ( $this->_entity === null )
		{
			$this->_response->msg = 'objectType_not_found';
			throw new ServiceBoxException('objectType_not_found');
		}
		
		$this->_entity->inject_data((array)$this->_request->data);
		
		$valid = $this->_validation->valid($this->_entity, $this->_request->data, $this->_request->action);
		
		$this->_response->data = $this->_request->data;
		
		if ( !$valid )
		{
			$this->_response->errors = $this->_validation->get_errors();
			throw new ServiceBoxException('validation_errors');
		}
	}
}