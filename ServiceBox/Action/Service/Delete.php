<?php

namespace ServiceBox\Action\Service;

use ServiceBox\Action\Entity as EntityAction;

class Delete extends EntityAction
{
	public function perform()
	{
		parent::perform();
		$this->_entity->id = $this->_request->objectID;
		$this->_response->success = $this->_storage->delete(
			$this->_entity, 
			array('id')
		);
	}
}