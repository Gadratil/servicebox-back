<?php

namespace ServiceBox\Action\Service;

use ServiceBox\Action\Entity as EntityAction;

class Insert extends EntityAction
{
	public function perform()
	{
		parent::perform();
		$id = $this->_storage->add($this->_entity);
					
		$this->_response->success = true;
		$this->_response->data = $id;
	}
}