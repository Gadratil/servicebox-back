<?php

namespace ServiceBox\Action\Service;

use ServiceBox\Action\Entity as EntityAction;

class Get extends EntityAction
{
	public function perform()
	{
		parent::perform();
		$this->_response->success = true;
		$this->_response->data = $this->_storage->get($this->_entity, array_keys($this->_request->data));
	}
}