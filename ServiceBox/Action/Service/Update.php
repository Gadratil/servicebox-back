<?php

namespace ServiceBox\Action\Service;

use ServiceBox\Action\Entity as EntityAction;

class Update extends EntityAction
{
	public function perform()
	{
		parent::perform();
		$this->_entity->id = $this->_request->objectID;
		$this->_response->success = $this->_storage->update(
			$this->_entity, 
			array_keys($this->_request->data), 
			array('id')
		);
	}
}