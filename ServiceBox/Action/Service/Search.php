<?php

namespace ServiceBox\Action\Service;

use ServiceBox\Action\Entity as EntityAction;

class Search extends EntityAction
{
	public function perform()
	{
		parent::perform();
		$get_total = false;
		if ( isset($this->_request->data['get_total']) )
		{
			$get_total = true;
			unset($this->_request->data['get_total']);
		}
		$this->_response->success = true;
		$this->_response->data = $this->_storage->search($entity, array_keys($this->_request->data), $get_total);
	}
}