<?php

namespace ServiceBox\Action\Service;

use ServiceBox\Action\Service as ServiceAction;

class Custom extends ServiceAction
{
	public function perform()
	{
		parent::perform();
		
		$this->_response->success = true;
		$this->_response->msg = 'custom_action';
		$this->_response->data = $this->_service->run_custom_function(
			$this->_request->customAction, 
			$this->_request->data
		);
	}
}