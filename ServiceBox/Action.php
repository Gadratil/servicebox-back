<?php

namespace ServiceBox;

use ServiceBox\Request as Request;
use ServiceBox\Response as Response;
use ServiceBox\Interfaces\Storage as StorageInterface;
use ServiceBox\Interfaces\Validation as ValidationInterface;
use ServiceBox\Auth\Auth as Auth;
use ServiceBox\Action\System\Login as Login;
use ServiceBox\Action\System\IsLoggedIn;
use ServiceBox\Action\System\Logout;
use ServiceBox\Action\Service\Custom;
use ServiceBox\Action\Service\Insert;
use ServiceBox\Action\Service\Update;
use ServiceBox\Action\Service\Delete;
use ServiceBox\Action\Service\Get;
use ServiceBox\Action\Service\Search;
use ServiceBox\ServiceBoxException;

abstract class Action
{
	public $_storage = null;
	
	public $_validation = null;
	
	public $_auth = null;
	
	public $_request = null;
	
	public $_response = null;
	
	public static function instance(Request $request, StorageInterface $storage, ValidationInterface $validation, Auth $auth, Response $response)
	{	
		if ( class_exists('ServiceBox\\Action\\System\\'.$request->action) )
		{
			$className = 'ServiceBox\\Action\\System\\'.$request->action;
			$instance = new $className();
			$instance->_storage = $storage;
			$instance->_validation = $validation;
			$instance->_auth = $auth;
			$instance->_request = $request;
			$instance->_response = $response;
			
			return $instance;
		}
		elseif( class_exists('ServiceBox\\Action\\Service\\'.$request->action) )
		{
			$className = 'ServiceBox\\Action\\Service\\'.$request->action;
			$instance = new $className();
			$instance->_storage = $storage;
			$instance->_validation = $validation;
			$instance->_auth = $auth;
			$instance->_request = $request;
			$instance->_response = $response;
			
			return $instance;
		}
		else
		{
			return 'action_not_known';
		}
	}
	
	public abstract function perform();
	
	public function get_response()
	{
		return $this->_response;
	}
}