<?php

namespace ServiceBox;

/**
 * Loads the services according to request
 */
class Plugger
{
	public function get_service($service_slug)
	{
		if ( file_exists(SERVICEBOX_ROOT.'services/'.$service_slug.'/'.$service_slug.'.php') )
		{
			return include_once(SERVICEBOX_ROOT.'services/'.$service_slug.'/'.$service_slug.'.php');
		}
		
		return null;
	}
}