<?php

namespace ServiceBox\Request;

use ServiceBox\Request;

class System extends Request
{
	public $action = '';
	public $data = array();
}