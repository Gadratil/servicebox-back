<?php

namespace ServiceBox\Request;

use ServiceBox\Request;

class Service extends Request
{
	public $action = '';
	public $data = array();
	public $objectID = null;
	public $objectType = '';
	public $customAction = '';
	public $files = array();
	public $service = '';
}