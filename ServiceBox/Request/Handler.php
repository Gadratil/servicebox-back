<?php

namespace ServiceBox\Request;

use ServiceBox\Interfaces\RequestHandler;
use ServiceBox\Request as Request;
use ServiceBox\Request\System as SystemRequest;
use ServiceBox\Request\Service as ServiceRequest;
use ServiceBox\Response as Response;
use ServiceBox\Response\General as GeneralResponse;
use ServiceBox\Interfaces\Storage as StorageInterface;
use ServiceBox\Interfaces\Validation as ValidationInterface;
use ServiceBox\Auth\Auth as Auth;
use ServiceBox\Action;
use ServiceBox\ServiceBoxException;

class Handler implements RequestHandler
{	

	private $_storage = null;
	
	private $_validation = null;
	
	private $_auth = null;
	
	private $_request = null;
	
	private $_response = null;
	
	public function __construct(StorageInterface $storage, ValidationInterface $validation, Auth $auth)
	{
		$this->_storage = $storage;
		$this->_validation = $validation;
		$this->_auth = $auth;
		$this->_response = new GeneralResponse();
	}

	public function handle(Request $request)
	{
		$this->_request = $request;
		
		$action = Action::instance($this->_request, $this->_storage, $this->_validation, $this->_auth, $this->_response);
		
		if ( is_string($action) )
		{
			$this->_response->msg = $action;
		}
		else
		{	
			try
			{
				$action->perform();
				
				$this->_response = $action->get_response();
			}
			catch(ServiceBoxException $e)
			{
				$this->_response->msg = $e->getMessage();
			}
		}
		
		return $this->_response;
	}
}