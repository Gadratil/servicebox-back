<?php

namespace ServiceBox\Interfaces;

use ServiceBox\Entity as Entity;

interface Validation
{
	/**
	 * Checks if the incoming data is valid
	 * @param ServiceBox\Entity $entity
	 * @param Array $data
	 * @param string $action Passed down from the incoming request
	 * @return boolean
	 */
	public function valid(Entity $entity, Array $data, $action);
	
	/**
	 * Returns the errors that got set upon validation
	 * @return array
	 */
	public function get_errors();
}