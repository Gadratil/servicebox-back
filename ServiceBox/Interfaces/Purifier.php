<?php

namespace ServiceBox\Interfaces;

/**
 * This is the request purifier, it does nothing else, just purifies incoming requests, cleaning everything that could be an attack
 */
interface Purifier
{
	/**
	 * Purifies the passed data, using a recursive strategy
	 * @param Array $data The incoming data
	 * @return Array
	 */
	public function purify(Array $data);
}