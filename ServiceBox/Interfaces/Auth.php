<?php

namespace ServiceBox\Interfaces;

interface Auth
{
	/**
	 * Logs in a user, creates session
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	public function login($username, $password);
	
	/**
	 * Logs out the current user
	 * @param string $session_id The id of the current session
	 * @return boolean
	 */
	public function logout($session_id);
	
	/**
	 * Checks if user is logged in
	 * @param string $session_id The id of the current session
	 */
	public function is_logged_in($session_id);
	
	/**
	 * Registers a new user
	 * @param string $username
	 * @param string $password
	 * @param string $email
	 * @return boolean
	 */
	public function register($username, $password, $email);
	
	public function get_errors();
}