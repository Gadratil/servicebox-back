<?php

namespace ServiceBox\Interfaces;

use ServiceBox\Request as Request;

/**
 * The class responsible for handling all requests
 */
interface RequestHandler
{
	/**
	 * Handles the incoming request
	 * @param ServiceBox\Request $request Contains the request, it can be $_POST or even a generated array
	 * @return ServiceBox\Response
	 */
	public function handle(Request $request);
}