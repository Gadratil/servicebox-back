<?php

namespace ServiceBox\Storage;

use ServiceBox\Interfaces\Storage as StorageInterface;
use ServiceBox\ServiceBoxException as ServiceBoxException;
use ServiceBox\Entity as Entity;
use DBHandler;

/**
 * The DB storage extension of the server message, uses mysqli PHP extension
*/ 
class DB implements StorageInterface
{
	private $_db = null;
	
	public function __construct(Array $config = array())
	{
		$this->_db = new DBHandler($config);
	}
	
	public function create_storage()
	{
	
	}
	
	public function add(Entity $entity)
	{
		$data = (array)$entity;
		
		unset($data['id']);
		
		return $this->_db->insert_data($entity->get_storage_name(), $data);
	}
	
	public function update(Entity $entity, Array $fields, Array $by_fields)
	{
		$data = (array)$entity;
		
		return $this->_db->update_data($entity->get_storage_name(), $data, $fields, $by_fields);
	}
	
	public function delete(Entity $entity, Array $by_fields)
	{
		$data = (array)$entity;
		
		return $this->_db->delete_data($entity->get_storage_name(), $data, $by_fields);
	}
	
	public function get(Entity $entity, Array $by_params, $limit = null, $offset = null)
	{
		$extra_sql = '';
		
		$data = (array)$entity;
		
		foreach( $data as $key => $value )
		{
			if ( in_array($key, $by_params) )
			{
				if ( !is_array($value) )
				{
					$extra_sql .= ' AND `'.$key.'` = '.((is_numeric($value))? $value : '"'.$value.'"');
				}
				else
				{
					$are_numbers = array_filter($value, 'is_numeric');
					if ( count($are_numbers) == count($value) )
					{
						$in = '('.implode(',', $value).')';
					}
					else
					{
						$in = '("'.implode('", "', $value).'")';
					}
					
					$extra_sql .= ' AND `'.$key.'` IN '.$in;
				}
			}
		}
		
		$extra_sql = rtrim($extra_sql, ',');
	
		$records = $this->_db->get_table_data($entity->get_storage_name(), $limit, $offset, $extra_sql);
		
		$tmp = array();
		foreach( $records as $row )
		{
			$class = get_class($entity);
			$e = new $class();
			$e->inject_data($row);
			
			$tmp[] = $e;
		}
		
		return $tmp;
	}
	
	public function get_total(Entity $entity, Array $by_params)
	{
		$extra_sql = '';
		
		$data = (array)$entity;
		
		foreach( $data as $key => $value )
		{
			if ( in_array($key, $by_params) )
			{
				if ( !is_array($value) )
				{
					$extra_sql .= ' AND `'.$key.'` = '.((is_numeric($value))? $value : '"'.$value.'"').',';
				}
				else
				{
					$are_numbers = array_filter($value, 'is_numeric');
					if ( count($are_numbers) == count($value) )
					{
						$in = '('.implode(',', $value).')';
					}
					else
					{
						$in = '("'.implode('", "', $value).'")';
					}
					
					$extra_sql .= ' AND `'.$key.'` IN '.$in.',';
				}
			}
		}
		
		$extra_sql = rtrim($extra_sql, ',');
		
		return $this->_db->get_table_total($entity->get_storage_name(), $extra_sql);
	}
	
	public function search(Entity $entity, Array $by_params, $get_total = false)
	{
		$extra_sql = '';
		
		$data = (array)$entity;
		
		foreach( $data as $key => $value )
		{
			if ( in_array($key, $by_params) )
			{
				$extra_sql .= ' AND `'.$key.'` LIKE "%'.$value.'%"';
			}
		}
		
		$extra_sql = rtrim($extra_sql, ',');
		
		if ( $get_total )
		{
			return $this->_db->get_table_total($entity->get_storage_name(), $extra_sql);
		}
		else
		{
			$records = $this->_db->get_table_data($entity->get_storage_name(), null, null, $extra_sql);
		
			$tmp = array();
			foreach( $records as $row )
			{
				$class = get_class($entity);
				$e = new $class();
				$e->inject_data($row);
				
				$tmp[] = $e;
			}
			
			return $tmp;
		}
	}
	
	public function exists()
	{
		
	}
	
	public function destroy_storage()
	{
		
	}
}