<?php

namespace ServiceBox\Auth;

use ServiceBox\Entity;
use ValidationWall\Rule\NotEmpty;

class User extends Entity
{
	public $id = null;
	public $username = '';
	public $email = '';
	public $password_hash = '';
	
	public function get_storage_name()
	{
		return 'system_users';
	}
	
	public function get_validation_definition()
	{
		return array(
			'login' => array(
				'username' => array(new NotEmpty()),
				'password' => array(new NotEmpty())
			),
			'delete' => array(),
		);
	}
}