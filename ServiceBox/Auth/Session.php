<?php

namespace ServiceBox\Auth;

use ServiceBox\Entity;

class Session extends Entity
{
	public $session_id = '';
	public $user_id = '';
	public $logged_in_at = '';
	public $valid_until = '';
	
	public function get_storage_name()
	{
		return 'system_session';
	}
	
	public function get_validation_definition()
	{
		return array(
			'all' => array()
		);
	}
}