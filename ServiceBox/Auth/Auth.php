<?php

namespace ServiceBox\Auth;

use ServiceBox\Interfaces\Auth as AuthInterface;
use ServiceBox\Interfaces\Storage as StorageInterface;
use ServiceBox\Auth\User as User;
use ServiceBox\Auth\Session as Session;
use PasswordHash;

class Auth implements AuthInterface
{
	private $_storage = null;
	
	private $_errors = array();
	
	private $_hasher = null;
	
	public function __construct(StorageInterface $storage)
	{
		$this->_storage = $storage;
		
		$this->_hasher = new PasswordHash(8, false);
	}
	
	public function login($username, $password)
	{
		$user = new User();
		$user->username = $username;
		
		$user = $this->_storage->get($user, array('username'));
		
		if ( empty($user) )
		{
			$this->_errors['username'] = 'No user with this username.';
			return false;
		}
		
		$user = $user[0];
		
		if ( !$this->_hasher->CheckPassword($password, $user->password_hash) )
		{
			$this->_errors['password'] = 'Incorrect password.';
			return false;
		}
		
		$session = new Session();
		$session->user_id = $user->id;
		
		$this->_storage->delete($session, array('user_id'));
		
		$session->session_id = base64_encode( time().'_'.$user->username );
		$session->logged_in_at = date('Y-m-d H:i:s');
		$session->valid_until = date('Y-m-d H:i:s', strtotime('+1 week'));
		
		$_SESSION['sess_id'] = $this->_storage->add($session);
		
		return true;
	}
	
	public function logout($session_id)
	{
		$session = new Session();
		$session->session_id = (int)$session_id;
		$this->_storage->delete($session, array('session_id'));
		
		unset($_SESSION['sess_id']);
	}
	
	public function is_logged_in($session_id)
	{
		$session = new Session();
		$session->session_id = (int)$session_id;
		
		$existing_session = $this->_storage->get($session, array('session_id'));
		
		if ( empty($existing_session) ) { return false; }
		
		$session = $existing_session[0];
		
		if ( strtotime($session->valid_until) < time() )
		{
			$this->_storage->delete($session, array('user_id'));
			return false;
		}
		
		return true;
	}
	
	public function register($username, $password, $email)
	{
		$user = new User();
		$user->username = $username;
		
		$existing_user = $this->_storage->get($user, array('username'));
		
		if ( !empty($existng_user) )
		{
			$this->_errors['username'] = 'Username already exists.';
			return false;
		}
		
		$user->password_hash = $this->_hasher->HashPassword($password);
		
		$user->email = $email;
		
		$this->_storage->add($user);
		
		return true;
	}
	
	public function get_errors()
	{
		return $this->_errors;
	}
}