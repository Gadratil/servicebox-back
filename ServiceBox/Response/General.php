<?php

namespace ServiceBox\Response;

use ServiceBox\Response as Response;

class General extends Response
{
	public $success = false;
	public $msg = '';
	public $data = null;
	public $errors = null;
}