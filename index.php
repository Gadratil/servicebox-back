<?php

session_start();
	
if (isset($_SERVER['HTTP_ORIGIN'])) 
{
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
//header('Content-Type: application/json');


require_once('require_autoloads.php');

$ServiceBox = new ServiceBox();
$ServiceBox->check_request();

echo 'ServiceBox ready';