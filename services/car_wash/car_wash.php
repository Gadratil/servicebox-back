<?php

use ServiceBox\Service as Service;

class CarWash extends Service
{
	public $slug = 'car_wash';
	
	public function SearchCars(Array $data)
	{
		$req_data = array(
			'type' => '',
			'q' => '',
			'makeID' => null
		);
		
		foreach ($req_data as $key => $val)
		{
			if ( array_key_exists($key, $data) )
			{
				$req_data[$key] = $data[$key];
			}
		}
		
		$cars_db = include_once(SERVICEBOX_ROOT.'services/'.$this->slug.'/vendor/carmakeandmodel/CarMakeModel.php');
		
		$s_type = $cars_db::S_BOTH;
		switch($req_data['type'])
		{
			case 'make':
				$s_type = $cars_db::S_MAKES;
				break;
			case 'model':
				$s_type = $cars_db::S_MODELS;
				break;
			case 'both':
				$s_type = $cars_db::S_BOTH;
				break;
		}
		
		return $cars_db->search($req_data['q'], $s_type, $req_data['makeID']);
	}
}

return new CarWash();