<?php
/**
 * Gets the cars and models into memory and provides with functions to get them
 * @author �rd�g Atilla
 */
class CarMakeModel 
{
	const S_BOTH = 0;
	const S_MAKES = 1;
	const S_MODELS = 2;
	
	private $makes = array();
	private $models = array();
	
	public function __construct()
	{
		if ( file_exists(dirname(__FILE__).'/Data.db_') )
		{
			$db_encoded = @file_get_contents(dirname(__FILE__).'/Data.db_');
			$tmp = unserialize( base64_decode($db_encoded) );
			
			$this->makes = $tmp['makes'];
			$this->models = $tmp['models'];
		}
	}
	
	/**
	 * Returns car makes
	 * @param int $id OPTIONAL the car make id, returns one record if given
	 * @return array
	 */
	public function getMakes($id = null) 
	{
		if ( $id !== null && array_key_exists($id, $this->makes) )
		{
			return $this->makes[$id];
		}			
		
		return array_values($this->makes);
	}
	
	/**
	 * Returns car models
	 * @param int $makeId OPTIONAL if given, returns the models of a given make
	 * @param int $id OPTIONAL if given, returns one entry. This has priority over makeId
	 * @return array
	 */
	public function getModels($makeId = null, $id = null)
	{
		if ( $id !== null )
		{
			$found = array();
			foreach ( $this->models as $makes )
			{
				if ( array_key_exists($id, $makes) )
				{
					$found = $makes[$id];
					break;
				}
			}
			
			if ( !empty($found) )
			{
				return $found;
			}
		}
		
		if ( $makeId !== null && array_key_exists($makeId, $this->models) ) 
		{
			return $this->models[$makeId];
		}
		
		$tmp = array();
		foreach ( $this->models as $models )
		{
			$tmp = array_merge($tmp, $models);
		}
		
		return $tmp;
	}
	
	/**
	 * Searches the database for car makes or models
	 * @param string $keyword The keyword to search for, it is case insensitive
	 * @param int $searchIn A constant to decide where to search. Choose from S_BOTH, S_MAKES, S_MODELS
	 * @param int $makeID OPTIONAL If given, it will only search in given make
	 * @return array
	 */
	public function search($keyword = '', $searchIn = self::S_BOTH, $makeID = null) 
	{
		$found = array();
		if ( $searchIn == self::S_BOTH || $searchIn == self::S_MAKES )
		{
			foreach( $this->makes as $make )
			{
				if ( stripos($make['name'], $keyword) !== false )
				{
					$make['type'] = 'make';
					$found[] = $make;
				}
			}
		}
		
		if ( $searchIn == self::S_BOTH || $searchIn == self::S_MODELS )
		{
			if ( $makeID !== null && array_key_exists($makeID, $this->models) )
			{
				$models = array($makeID => $this->models[$makeID]);
			}
			else
			{
				$models = $this->models;
			}
			
			foreach( $models as $make )
			{
				foreach ( $make as $model )
				{
					if ( stripos($model['name'], $keyword) !== false )
					{
						$model['type'] = 'model';
						$found[] = $model;
					}
				}
			}
		}
		
		return $found;
	}
}

$CMM = new CarMakeModel();

return $CMM;