<?php

use ServiceBox\Validation\Rule\AlphaNumeric;
use ServiceBox\Entity as Entity;
use ValidationWall\Rule\NotEmpty;

class Delegate extends Entity
{
	public $id;
	public $delegate_name;
	public $delegate_phone;
	
	public function get_storage_name()
	{
		return 'car_wash_delegates';
	}
	
	public function get_validation_definition()
	{	
		$notEmpty = new NotEmpty();
		$alphaNumeric = new AlphaNumeric();
		
		return array(
			'get' => array(),
			'delete' => array(),
			'search' => array(),
			'all' => array(
				'delegate_name' => array($notEmpty),
				'delegate_phone' => array($notEmpty, $alphaNumeric)
			)
		);
	}
}

return new Delegate();