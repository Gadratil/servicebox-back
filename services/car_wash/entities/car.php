<?php

use ServiceBox\Validation\Rule\AlphaNumeric;
use ServiceBox\Entity as Entity;
use ValidationWall\Rule\NotEmpty;

class Car extends Entity
{
	public $id;
	public $plate_number;
	public $car_make;
	public $car_type;
	public $inside_wash;
	public $outside_wash;
	public $date_created;
	public $status;
	public $delegate_id;
	
	public function get_storage_name()
	{
		return 'car_wash_cars';
	}
	
	public function get_validation_definition()
	{	
		$notEmpty = new NotEmpty();
		$alphaNumeric = new AlphaNumeric();
		
		return array(
			'Get' => array(),
			'Delete' => array(),
			'Search' => array(),
			'Update' => array(
				'status' => array($notEmpty)
			),
			'all' => array(
				'plate_number' => array($notEmpty),
				'car_make' => array($notEmpty),
				'car_type' => array($notEmpty)
			)
		);
	}
}

return new Car();