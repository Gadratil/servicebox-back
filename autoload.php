<?php
/**
 * Autoload file that needs to be loaded to use ServiceBox
 * In this autoload type the file name has to match the class name
 * Since we use namespaces, we always have to add "use" if we want to use a class
 */

if (!defined('SERVICEBOX_ROOT')) {
    define('SERVICEBOX_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
}

spl_autoload_register('servicebox_autoload');

function servicebox_autoload($class)
{	
	if ( class_exists($class,FALSE) ) {
		// Already loaded
		return FALSE;
	}
	
	$class = str_replace('\\', '/', $class);

	if ( file_exists(SERVICEBOX_ROOT.$class.'.php') )
	{
		require(SERVICEBOX_ROOT.$class.'.php');
	}

	return false;
}