<?php

require_once('autoload.php');
require_once(SERVICEBOX_ROOT.'vendor/DBHandler/DBHandler.php');
require_once(SERVICEBOX_ROOT.'vendor/ValidationWall-master/autoload.php');
require_once(SERVICEBOX_ROOT.'vendor/htmlpurifier-4.6.0/library/HTMLPurifier.auto.php');
require_once(SERVICEBOX_ROOT.'vendor/phpass-0.3/PasswordHash.php');